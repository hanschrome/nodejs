const utils = require('../utils');
const axios = require('axios');
var fs = require('fs');
var imageDir = 'images';

function createJWT(userData, reqOrigin) {
    let options = {
        issuer: utils.appConstants.JWTOPTIONS.issuer,
        subject: utils.appConstants.JWTOPTIONS.subject,
        audience: reqOrigin,
    }
    return utils.jwtUtil.sign({ userId: userData._id }, options);
}

class LoginFacade {
    async login(authCode, reqOrigin) {
        const tokens = await utils.googleUtil.getGoogleTokenFromAuthCode(authCode);
        const userData = await utils.googleUtil.getGoogleAccountData(tokens);
        userData.createdAt = new Date().valueOf();
        userData.updatedAt = new Date().valueOf();
        const dbClient = await utils.mongoUtil.connectDB(utils.appConstants.DB_CONNECTION_URL);
        const existingUser = await utils.mongoUtil.findData(dbClient, utils.appConstants.BASE_DB_NAME,
            utils.appConstants.DB_COLLECTIONS.USER, { 'id': userData.id });
        let loggedInUserData; let jwtToken;
        if (existingUser[0] && existingUser[0]._id) {
            loggedInUserData = existingUser[0];
        }
        else {
            //Note: for the first when user logs in, we get its image from google and save it in our directory for future use
            let imageUrl = await getImageUrl(userData.picture, userData.id, imageDir);
            userData.picture = imageUrl;
            await utils.mongoUtil.insertData(dbClient, utils.appConstants.BASE_DB_NAME,
                utils.appConstants.DB_COLLECTIONS.USER, [userData]);
            loggedInUserData = userData;
        }
        utils.mongoUtil.closeDBConnection(dbClient)
        jwtToken = createJWT(loggedInUserData, reqOrigin);
        return {
            accessToken: jwtToken,
            user: loggedInUserData
        };
    }
}

//Here we save the image on our server and return its url
async function getImageUrl(imageUrl, imageName, dir) {
    const imageBuffer = await axios.get(imageUrl, { responseType: 'arraybuffer' });
    var base64Data = new Buffer(imageBuffer.data, 'binary').toString('base64')
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    let imagePath = dir + "/" + imageName + ".png";
    fs.writeFileSync(imagePath, base64Data, 'base64');
    return imagePath;
}

module.exports = new LoginFacade();