const app = require('../app').app;
const utils = require('../utils');
var dir = 'images';
var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './' + dir);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
var upload = multer({ storage: storage });

app.get('/user', async (req, res, next) => {
    try {
        const dbClient = await utils.mongoUtil.connectDB(utils.appConstants.DB_CONNECTION_URL);
        const userData = await utils.mongoUtil.findData(dbClient, utils.appConstants.BASE_DB_NAME,
            utils.appConstants.DB_COLLECTIONS.USER,
            { '_id': utils.mongoUtil.convertStringIdToMongoId(req.jwtData.userId) });
        utils.mongoUtil.closeDBConnection(dbClient);
        res.send(userData[0]);
    }
    catch (err) {
        next(err);
    }
})

app.post('/user', async (req, res, next) => {
    try {
        if (!req.body) {
            throw new Error('Invalid Request');
        }
        else {
            const dbClient = await utils.mongoUtil.connectDB(utils.appConstants.DB_CONNECTION_URL);
            await utils.mongoUtil.updateData(dbClient, utils.appConstants.BASE_DB_NAME,
                utils.appConstants.DB_COLLECTIONS.USER,
                { '_id': utils.mongoUtil.convertStringIdToMongoId(req.jwtData.userId) },
                req.body);
            utils.mongoUtil.closeDBConnection(dbClient);
            res.send({});
        }
    }
    catch (err) {
        next(err);
    }
})

//This api is used to upload image in our server directory using Multer
app.post('/file_upload', upload.single('imageFile'), async (req, res, next) => {
    try {
        console.log(req.file);
        const dbClient = await utils.mongoUtil.connectDB(utils.appConstants.DB_CONNECTION_URL);
        await utils.mongoUtil.updateData(dbClient, utils.appConstants.BASE_DB_NAME,
            utils.appConstants.DB_COLLECTIONS.USER,
            { '_id': utils.mongoUtil.convertStringIdToMongoId(req.jwtData.userId) },
            { 'picture': dir + '/' + req.file.originalname });
        utils.mongoUtil.closeDBConnection(dbClient);
        console.log("success");
        res.send({});
    }
    catch (err) {
        next(err);
    }
});