const app = require('../app').app;
const utils = require('../utils');

app.get('/bills', async (req, res, next) => {
    try {
        const dbClient = await utils.mongoUtil.connectDB(utils.appConstants.DB_CONNECTION_URL);
        const bills = await utils.mongoUtil.findData(dbClient, utils.appConstants.BASE_DB_NAME,
            utils.appConstants.DB_COLLECTIONS.BILL,
            { 'userId': utils.mongoUtil.convertStringIdToMongoId(req.jwtData.userId) });
        utils.mongoUtil.closeDBConnection(dbClient);
        res.send(bills);
    }
    catch (err) {
        next(err);
    }
})

app.put('/bill', async (req, res, next) => {
    try {
        if (!(req.query.id || req.body)) {
            throw new Error('Invalid Request');
        }
        else {
            const dbClient = await utils.mongoUtil.connectDB(utils.appConstants.DB_CONNECTION_URL);
            await utils.mongoUtil.updateData(dbClient, utils.appConstants.BASE_DB_NAME,
                utils.appConstants.DB_COLLECTIONS.BILL,
                { '_id': utils.mongoUtil.convertStringIdToMongoId(req.query.id) },
                req.body);
            utils.mongoUtil.closeDBConnection(dbClient);
            res.send({});
        }
    }
    catch (err) {
        next(err);
    }
})