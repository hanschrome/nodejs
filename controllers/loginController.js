const app = require('../app').app;
const utils = require('../utils');
const facades = require('../facades');

app.get('/url', (req, res, next) => {
    try {
        res.send(utils.googleUtil.urlGoogle());
    }
    catch (err) {
        next(err);
    }
});

app.post('/login', async (req, res, next) => {
    try {
        if (!(req.body && req.body.code)) {
            throw new Error('No Auth Code');
        }
        else {
            const userData = await facades.loginFacade.login(req.body.code, req.headers.origin);
            return res.send(userData);
        }
    }
    catch (err) {
        next(err);
    }
})