const app = require('../app').app;
const ipn = require('paypal-ipn');
const utils = require('../utils');

app.post('/paypal', function (req, res) {
    var params = req.body;
    res.sendStatus(200);
    ipn.verify(params, { 'allow_sandbox': true }, async (err, msg) => {
        if (err) {
            console.log("Error:" + err);
        } else {
            res.end();
            if (params.payment_status == 'Completed') {
                //Payment has been confirmed as completed
                let body = {
                    status: 'PAID'
                }
                const dbClient = await utils.mongoUtil.connectDB(utils.appConstants.DB_CONNECTION_URL);
                await utils.mongoUtil.updateData(dbClient, utils.appConstants.BASE_DB_NAME,
                    utils.appConstants.DB_COLLECTIONS.BILL,
                    { '_id': utils.mongoUtil.convertStringIdToMongoId(params.custom) },
                    body);
                utils.mongoUtil.closeDBConnection(dbClient);
                console.log('paypal payment success');
            }
        }
    });
});