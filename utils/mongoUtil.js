const mongo = require('mongodb');

class MongoUtil {
    async connectDB(url, options) {
        return mongo.MongoClient.connect(url, options);
    }

    async insertData(client, dbName, collectionName, data, options) {
        // Get the documents collection
        const collection = client.db(dbName).collection(collectionName);
        // Insert some documents
        return collection.insertMany(data, options);
    }

    async findData(client, dbName, collectionName, queryObj, options) {
        // Get the documents collection
        const collection = client.db(dbName).collection(collectionName);
        // Find some documents
        return collection.find(queryObj, options).toArray();
    }

    async updateData(client, dbName, collectionName, updateCondition, updateObj, options) {
        // Get the documents collection
        const collection = client.db(dbName).collection(collectionName);
        // Find some documents
        return collection.updateOne(updateCondition, { $set: updateObj }, options);
    }

    closeDBConnection(client) {
        client.close();
    }

    convertStringIdToMongoId(id) {
        return mongo.ObjectID(id);
    }
}

module.exports = new MongoUtil();
