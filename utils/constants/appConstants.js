module.exports = Object.freeze({
    JWTOPTIONS: {
        issuer: 'vibhor.vp@gmail.com',
        subject: 'subject',
        expiresIn: '1hr',
        algorithm: 'RS256'
    },
    BASE_DB_NAME: 'BASE_PROJECT_DB',
    DB_CONNECTION_URL: 'mongodb://localhost:27017',
    DB_COLLECTIONS: {
        USER: 'User',
        BILL: 'Bill'
    }
})