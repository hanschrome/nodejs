const { google } = require('googleapis');

const googleConfig = {
    clientId: '853138464982-7bkeh23f3rfdeave4b3te648odmcs8ld.apps.googleusercontent.com', // generate your own clientId using 'https://console.developers.google.com/apis/credentials'
    clientSecret: '8xdqyWbdUJZjoGlPXjruUONA', // generate your own clientSecret using 'https://console.developers.google.com/apis/credentials'
    redirect: 'http://localhost:4000/oauth' // this must match your google api settings
};
/**
 * This scope tells google what information we want to request.
 */
const defaultScope = [
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/userinfo.email',
];

class GoogleUtil {
    /**
     * Create the google auth object which gives us access to talk to google's apis.
     */
    createConnection() {
        return new google.auth.OAuth2(
            googleConfig.clientId,
            googleConfig.clientSecret,
            googleConfig.redirect
        );
    }

    /**
 * Get a url which will open the google sign-in page and request access to the scope provided (such as calendar events).
 */
    getConnectionUrl(auth) {
        return auth.generateAuthUrl({
            access_type: 'offline',
            prompt: 'consent', // access type and approval prompt will force a new refresh token to be made each time user signs in
            scope: defaultScope
        });
    }

    /**
     * Create the google url to be sent to the client.
     */
    urlGoogle() {
        const auth = this.createConnection();
        const url = this.getConnectionUrl(auth);
        return url;
    }

    /**
    * Take the "code" parameter which Google gives us once when the user logs in, then get the user's email and id.
    */
    async getGoogleTokenFromAuthCode(code) {
        const auth = this.createConnection();
        const data = await auth.getToken(code);
        const tokens = data.tokens;
        return tokens;
    }

    /**
    * Get the user's email and id, using the tokens/token that was previously fetched
    */
    async getGoogleAccountData(tokens) {
        const auth = this.createConnection();
        auth.setCredentials(tokens);
        const oAuth = this.getGoogleOAuthApi(auth);
        const userData = await oAuth.userinfo.get();
        return {
            id: userData.data.id,
            email: userData.data.email,
            firstName: userData.data.name.split(' ')[0],
            lastName: userData.data.name.split(' ')[1],
            picture: userData.data.picture,
        };
    }

    /**
    * Allows to connect to Google OAuth api and get basic user data
    */
    getGoogleOAuthApi(auth) {
        return google.oauth2({ version: 'v2', auth });
    }
}

module.exports = new GoogleUtil();