const fs = require('fs');
const jwt = require('jsonwebtoken');
var path = require('path');
const appConstants = require('./constants/appConstants');

// use 'utf8' to get string instead of byte array  (512 bit key)
var privateKEY = fs.readFileSync(path.resolve('keys/private.key'), 'utf8');
var publicKEY = fs.readFileSync(path.resolve('keys/public.key'), 'utf8');

class JWTUtil {
    /**
    * Synchronously sign the given payload into a JSON Web Token string payload - Payload to sign, could be an literal, buffer or string secretOrPrivateKey - Either the secret for HMAC algorithms, or the PEM encoded private key for RSA and ECDSA. [options] - Options for the signature returns - The JSON Web Token string
    */
    sign(payload, options) {
        var signOptions = {
            issuer: options.issuer,
            subject: options.subject,
            audience: options.audience,
            expiresIn: appConstants.JWTOPTIONS.expiresIn,
            algorithm: appConstants.JWTOPTIONS.algorithm
        };
        return jwt.sign(payload, privateKEY, signOptions);
    }

    /**
    * Synchronously verify given token using a secret or a public key to get a decoded token token - JWT string to verify secretOrPublicKey - Either the secret for HMAC algorithms, or the PEM encoded public key for RSA and ECDSA. [options] - Options for the verification returns - The decoded token.
    */
    verify(token, options) {
        var verifyOptions = {
            issuer: options.issuer,
            subject: options.subject,
            audience: options.audience,
            expiresIn: appConstants.JWTOPTIONS.expiresIn,
            algorithms: [appConstants.JWTOPTIONS.algorithm]
        };
        return jwt.verify(token, publicKEY, verifyOptions);
    }

    decode(token) {
        //returns null if token is invalid
        return jwt.decode(token, { complete: true });
    }
}

module.exports = new JWTUtil();