const utils = require('../utils');
const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    try {
        if ((req.url.indexOf('/url') != -1 || req.url.indexOf('/login') != -1 || req.url.indexOf('/paypal') != -1
            || req.url.indexOf('/images') != -1)) {
            next();
            return;
        }
        if (!req.headers.authorization) {
            throw new Error('Unauthorized User');
        }
        else {
            let options = {
                issuer: utils.appConstants.JWTOPTIONS.issuer,
                subject: utils.appConstants.JWTOPTIONS.subject,
                audience: req.headers.origin,
            }
            req.jwtData = utils.jwtUtil.verify(req.headers.authorization.split(' ')[1], options);
            next();
        }
    }
    catch (err) {
        next(err);
    }
}